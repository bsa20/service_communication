﻿using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;
using System;
using System.Threading;

namespace Ponger
{
    class Program
    {
        private static SenderSettings senderSettings;
        private static QueueService queueService;

        static void Main(string[] args)
        {
            using var prodFactory = new MessageFactory("localhost");
            var queueSettings = prodFactory.GetQueueSettings("pong_queue");
            senderSettings = prodFactory.GetSenderSettings("ping_queue", "pong", true);

            queueService = new QueueService();
            queueService.ReceiveMessage += GetMessage;
            queueService.SendMessage += SendMessage;

            Console.WriteLine("Ponger started");

            queueService.ListenQueue(queueSettings, true);

            Console.ReadLine();
        }

        private static void SendMessage(string message)
        {
            Console.WriteLine($" [x] Sent {message}  at {DateTime.Now}");
        }

        private static void GetMessage(string message)
        {
            Console.WriteLine($" [x] Recieved {message} at {DateTime.Now}");

            Thread.Sleep(2500);

            SendMessageToQueue();
        }

        private static void SendMessageToQueue()
        {
            queueService.SendMessageToQueue(senderSettings);
        }
    }
}
